import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: 'empleados',
    loadChildren: () => import('./components/empleados/empleados.module').then(m => m.EmpleadosModule)
  },
  {
    path: '',
    loadChildren: () => import('./components/principal/principal.module').then(m => m.PrincipalModule)
  },
  { path: '**', pathMatch: 'full', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
