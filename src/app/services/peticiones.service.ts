import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  private baseUrl = 'http://localhost:8002';

  constructor(private http: HttpClient) { }

  public departamentosPromedio(): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/departamento/promedioDepartamento`, {}
      )
      .pipe(map((response) => response, error => error));
  }


  public informacionEmpleado(): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/empleados/obtenerEmpleados`, {}
      )
      .pipe(map((response) => response, error => error));
  }


  public crearEmpleado(data: any): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/empleados/agregarEmpleado`, data
      )
      .pipe(map((response) => response, error => error));
  }

  public actualizarEmpleado(data: any): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/empleados/actualizarEmpleado`, data
      )
      .pipe(map((response) => response, error => error));
  }



  public asociarFuncionEmpleado(data: any): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/empleados/asociarFuncionEmpleado`, data
      )
      .pipe(map((response) => response, error => error));
  }
  public obtnerFunciones(data: any): Observable<any> {
    return this.http
      .post(
        `${this.baseUrl}/funciones/obtenerFunciones`, data
      )
      .pipe(map((response) => response, error => error));
  }


}

