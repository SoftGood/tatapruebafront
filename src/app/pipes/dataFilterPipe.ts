import * as _ from 'lodash';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dataFilter'
})
export class DataFilterPipe implements PipeTransform {

    transform(array: any[], query: string, columna: string): any {
        console.log("array", array);
        if (query && columna) {
            return _.filter(array, (row: any) => row[0][columna].toString().toUpperCase().indexOf(query.toUpperCase()) > -1);
        }
        return array;
    }
}