import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmpleadosRoutingModule } from './empleados-routing.module';
import { EmpleadosComponent } from './empleados.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { DataFilterPipe } from 'src/app/pipes/dataFilterPipe';

@NgModule({
  declarations: [EmpleadosComponent, DataFilterPipe],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class EmpleadosModule { }
