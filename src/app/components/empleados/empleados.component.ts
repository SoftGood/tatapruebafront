import { Component, OnInit } from '@angular/core';
import { PeticionesService } from 'src/app/services/peticiones.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  public listaDepartamentos = [];
  private listaEmpleadoAux = [];
  public listaEmpleado = [];
  public dataEmpleado = {
    numerodocumento: '',
    nombre: '',
    apellidos: '',
    telefono: '',
    salario: 0,
    correo: '',
    departamento: { codigo: '', nombre: '' },
    activo: true,
  };
  private keyEmpleado: string[];
  public isModificar: boolean;
  public buscarPorId: string;
  public itemSelect = 1;
  public isLoader: boolean;
  public funciones = [];
  public funcionModel: string;

  constructor(
    private peticionesService: PeticionesService,
    private router: Router) { }

  ngOnInit() {
    this.obtenerInformacion();
    this.keyEmpleado = Object.keys(this.dataEmpleado);
  }

  private obtenerInformacion(): void {
    try {
      this.peticionesService.informacionEmpleado().subscribe((response: any) => {
        if (response.exitoso) {
          this.listaEmpleado = response.data[0];
          this.listaEmpleadoAux = this.convertirObjeto(response.data[0]);
          this.listaDepartamentos = response.data[1];
        } else {
          this.listaDepartamentos = response.data[0];
        }


      }, (err) => {
        setTimeout(() => {
          this.router.navigate(['']);
          this.mensajeFinal('Información', 'No se pudo establecer conexion con el servidor.');
        }, 500);
        console.error('Error de conexion', err);
      });
    } catch (e) {
      console.error('error', e);
    }
  }

  public agregarEmpleado(): void {
    try {

      if (this.validaFormulario()) {

        this.peticionesService.crearEmpleado(this.dataEmpleado).subscribe((response: any) => {
          if (response.exitoso) {
            const val = [];
            val.push(this.dataEmpleado);
            val.push([]);
            this.listaEmpleado.push(val);
            this.listaEmpleadoAux = this.listaEmpleado;
            this.mensajeFinal('Información', response.mensaje);
            this.limpiar();
          } else {
            this.mensajeFinal('Información', response.mensaje);
          }

        }, (err) => {
          this.mensajeFinal('Información', 'No se pudo establecer conexion con el servidor.');
          console.error('Error de conexion', err);
        });
      }
    } catch (e) {
      console.error('error', e);
    }
  }


  public irModificar(item: any): void {
    this.limpiar();

    this.dataEmpleado = item;
    this.isModificar = true;
    const selector = $('#formInfo').offset().top;
    $('html, body').animate({ scrollTop: `${selector}px` }, 1000);

  }

  public actualizarEmpleado(): void {
    try {

      if (this.validaFormulario()) {

        this.peticionesService.actualizarEmpleado(this.dataEmpleado).subscribe((response: any) => {
          if (response.exitoso) {
            this.mensajeFinal('Información', response.mensaje);
            this.isModificar = false;
            this.limpiar();
          } else {
            this.mensajeFinal('Información', response.mensaje);
          }

        }, (err) => {
          this.mensajeFinal('Información', 'No se pudo establecer conexion con el servidor.');
          console.error('Error de conexion', err);
        });
      }
    } catch (e) {
      console.error('error', e);
    }
  }

  private validaFormulario(): boolean {

    let exitoso = true;
    this.confirmarDepartamento();

    for (const campo of this.keyEmpleado) {
      if (this.validaVacio(this.dataEmpleado[campo])) {
        this.mensajeFinal(
          'Información', `El campo <span><b>
          ${this.replace(campo.toUpperCase(), 'NUMERODOCUMENTO', 'NUMERO DE DOCUMENTO')}
          </b></span> no puede quedar vacio.`,
          campo);
        exitoso = false;
        break;
      }
    }

    return exitoso;
  }


  public verFunciones(data: any) {
    Swal.fire({
      width: 1200,
      showCloseButton: true,
      showConfirmButton: false,
      focusConfirm: false,
      allowOutsideClick: false,
      html: `

      <div class="top20">
        <p class="center"><b>DETALLE DE FUNCIONES </b></p>
     </div>
      <div class="top20">

      <hr>
      <table class="table top20 ">
      <thead class="thead-light table-hover">
          <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>Departamento</th>
          </tr>
      </thead>
      <tbody>
      ${ data.length > 0 ? this.armarTabla(data) : 'Sin funciones asignadas.'}
      </tbody>
      </table>


      `,
    });
  }


  public armarTabla(lista: any) {
    let opt = '';

    for (const item of lista) {
      opt = opt + `
      <tr>
      <td> ${item.id}</td>
      <td> ${item.nombre}</td>
      <td> ${item.descripcion}</td>
      <td> ${item.departamento.nombre}</td>
      </tr>
      `;
    }


    return opt;
  }

  public tabOpcion(opcion: any) {
    this.itemSelect = opcion;

    if (opcion === 2) {
      this.obtenerFunciones();
    }
  }

  private obtenerFunciones(): void {
    try {
      this.isLoader = true;
      const envioFinal = {
        departamento: this.dataEmpleado.departamento.codigo
      };
      this.peticionesService.obtnerFunciones(envioFinal)
        .subscribe((response: any) => {
          this.funciones = response;
          this.isLoader = false;
        }, (err) => {
          this.mensajeFinal('Información', 'No se pudo establecer conexion con el servidor.');
          console.error('Error de conexion', err);
          this.isLoader = false;
        });

    } catch (e) {
      console.error('error', e);
      this.isLoader = false;
    }
  }


  public asociarFunciones(): void {
    try {

      if (`${this.funcionModel}` !== '') {

        const envioFinal = {
          funcion: this.funcionModel,
          empleado: this.dataEmpleado.numerodocumento
        };

        this.peticionesService.asociarFuncionEmpleado(envioFinal).subscribe((response: any) => {
          this.mensajeFinal('Información', response.mensaje);
        }, (err) => {
          this.mensajeFinal('Información', 'No se pudo establecer conexion con el servidor.');
          console.error('Error de conexion', err);
        });
      } else {
        this.mensajeFinal('Información', 'Debe seleccionar una función');
      }
    } catch (e) {
      console.error('error', e);
    }
  }


  private validaVacio(info: string): boolean {
    let valido = false;
    if (info) {
      if (!info.toString().trim()) {
        valido = true;
      }
    }

    return valido;
  }


  public limpiar(): void {

    this.dataEmpleado = {
      numerodocumento: '',
      nombre: '',
      apellidos: '',
      telefono: '',
      salario: 0,
      correo: '',
      departamento: { codigo: '', nombre: '' },
      activo: true,
    };

    if (this.isModificar) {
      this.listaEmpleado = this.listaEmpleadoAux;
    }
    this.itemSelect = 1;

    this.isModificar = false;

  }

  private mensajeFinal(titulo: string, mensaje: string, focu?: string): void {
    Swal.fire({
      icon: 'info',
      title: titulo,
      html: mensaje,
    }).then(() => {
      if (focu) {
        setTimeout(() => {
          document.getElementById(focu).focus();
        }, 350);
      }
    });
  }

  private replace(texto: any, detexto: string, atexto: string): string {
    return texto.replace(detexto, atexto);
  }


  public confirmarDepartamento(): void {
    this.dataEmpleado.departamento.codigo = (document.getElementById('codigo') as HTMLInputElement).value;

    const seleccionado = this.listaDepartamentos.filter(data => data.codigo.toString() === this.dataEmpleado.departamento.codigo)[0];
    this.dataEmpleado.departamento = seleccionado;
  }


  private convertirObjeto(valor: any): any {

    let respuesta = [];
    if (valor) {
      respuesta = JSON.parse(JSON.stringify(valor));
    }
    return respuesta;
  }

}
