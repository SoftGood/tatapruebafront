import { Component, OnInit } from '@angular/core';
import { PeticionesService } from 'src/app/services/peticiones.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  public departamentos = [];
  constructor(private peticionesService: PeticionesService) { }

  ngOnInit() {
    this.obtenerPromedio();
  }

  private obtenerPromedio(): void {
    this.peticionesService.departamentosPromedio().subscribe((info: any) => {
      this.departamentos = info;
    }, (err: any) => {
      Swal.fire({
        icon: 'info',
        title: 'Información',
        html: 'No se pudo establecer conexion con el servidor.',
      });
      console.log('err', err);
    });
  }

}
